import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/best.seller.week.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/cardinfo.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/catigory.group.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/flash.sales.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/header.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/horizon.menu.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/privilege.customer.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/promote.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/recommend.product.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/slide.image.widget.dart';
import 'package:flutter_shopee_clone/utils/navigationbar.bottom/navigationbar.dart';
import 'package:flutter_shopee_clone/utils/pulltorefresh/pulltorefresh.dart';
import 'package:uuid/uuid.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  bool loading = false;
  TrackingScrollController trackingScrollController =
      TrackingScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      // appBar: AppBar(/
      //   title: Text('หน้าแรก'),
      // ),
      body: PullToRefresh(
        child: Stack(
          children: [
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              controller: trackingScrollController,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Stack(
                    clipBehavior: Clip.none,
                    alignment: Alignment.bottomCenter,
                    children: [
                      SlideImageWidget(
                        aspectRatio: 1.777,
                        buttonPadding: 55,
                        indicatorTop: 125,
                        urlImageList: [
                          "assets/images/baner1.jpg",
                          "assets/images/baner2.jpg",
                          "assets/images/baner3.jpg",
                          "assets/images/baner4.jpg",
                          "assets/images/baner5.jpg"
                        ],
                      ),
                      CardInfoWidget()
                    ],
                  ),
                  HorizontalMenuWidget(),
                  PrivilegeCustomerWidget(),
                  FlashSalesWidget(
                    products: [
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      ""
                    ],
                    maxItem: 5,
                  ),
                  PromoteWidget(),
                  BestSellerWeeklyWidget(
                    products: [
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      "",
                      ""
                    ],
                    maxItem: 10,
                  ),
                  CatigoryGroupWidget(),
                ],
              ),
            ),
            HeaderMainWidget(
                trackingScrollController: trackingScrollController),
          ],
        ),
        onRefresh: () async {
          setState(() {
            loading = true;
          });
          print('loading**** $loading');
          await Future.delayed(Duration(milliseconds: 3000));
          setState(() {
            loading = false;
          });

          print('loading**** $loading');
        },
      ),
    );
  }
}
