import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/utils/navigationbar.bottom/navigationbar.dart';
import 'package:uuid/uuid.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  void _addDataFirebase() {
    final uuid = Uuid();
    final faker = new Faker();
    FirebaseFirestore.instance
        .collection('products')
        .doc(uuid.v1())
        .set({"id": faker.person.name()});
  }

  int menuselected = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feed'),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('products').snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (!snapshot.hasData) {
            return const SizedBox.shrink();
          }
          return ListView.builder(
              itemCount: snapshot.data.docs.length,
              itemBuilder: (BuildContext context, int index) {
                final docData = snapshot.data.docs[index].data();
                return ListTile(title: Text(docData['id'].toString()));
              });
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _addDataFirebase,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
