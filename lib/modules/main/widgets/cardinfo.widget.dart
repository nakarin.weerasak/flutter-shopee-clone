import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/custom.color.dart';
import 'package:flutter_shopee_clone/fontsize.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:fluttericon/iconic_icons.dart';
import 'package:fluttericon/meteocons_icons.dart';

class CardInfoWidget extends StatelessWidget {
  const CardInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      padding: EdgeInsets.only(left: 8, right: 8),
      child: Card(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                padding:
                    EdgeInsets.only(left: 10, right: 5, bottom: 10, top: 10),
                child: Icon(FontAwesome.qrcode),
              ),
            ),
            Container(
              height: double.infinity,
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: VerticalDivider(
                thickness: 1,
                color: Colors.grey.shade400,
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                height: double.infinity,
                padding: EdgeInsets.only(top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 2, right: 2),
                            child: Icon(
                              Icons.payment,
                              color: Colors.deepOrange,
                            ),
                          ),
                          Text('ShopeePay')
                        ],
                      ),
                    ),
                    Container(
                      child: Text(
                        'ข้าสู่ระบบเพื่อเช็คยอดเงินใน',
                        style: TextStyle(
                            fontSize: FontSi.XS, color: Colors.grey.shade500),
                      ),
                    ),
                    Container(
                      child: Text('ShopeePay ของคุณ',
                          style: TextStyle(
                              fontSize: FontSi.XS,
                              color: Colors.grey.shade500)),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: double.infinity,
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: VerticalDivider(
                thickness: 1,
                color: Colors.grey.shade400,
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                height: double.infinity,
                padding: EdgeInsets.only(top: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 2, right: 2),
                            child: Icon(
                              Icons.circle,
                              color: Colors.deepOrange,
                            ),
                          ),
                          Text('Shopee Coins')
                        ],
                      ),
                    ),
                    Container(
                      child: Text(
                        'ข้าสู่ระบบเพื่อเช็คยอด',
                        style: TextStyle(
                            fontSize: FontSi.XS, color: Colors.grey.shade500),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
