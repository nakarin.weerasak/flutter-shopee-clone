import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class SlideImageWidget extends StatefulWidget {
  List<String> urlImageList = [];
  double buttonPadding;
  double aspectRatio = 1.77;
  double indicatorBottom;
  double indicatorLeft;
  double indicatorRigth;
  double indicatorTop;
  SlideImageWidget(
      {required this.urlImageList,
      required this.aspectRatio,
      required this.buttonPadding,
      this.indicatorBottom = 0,
      this.indicatorTop = 110,
      this.indicatorLeft = 10,
      this.indicatorRigth = 0});

  @override
  _SlideImageWidgetState createState() => _SlideImageWidgetState();
}

class _SlideImageWidgetState extends State<SlideImageWidget> {
  late int _currentImgIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentImgIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [imageSlide(), indicatorSlide()],
    );
  }

  imageSlide() => Container(
        margin: EdgeInsets.only(bottom: widget.buttonPadding),
        width: double.infinity,
        child: CarouselSlider(
          options: CarouselOptions(
              aspectRatio: widget.aspectRatio,
              viewportFraction: 1.0,
              autoPlay: true,
              onPageChanged: (index, reson) {
                Future.delayed(Duration(milliseconds: 180), () {
                  setState(() {
                    _currentImgIndex = index;
                  });
                });
              }),
          items: widget.urlImageList.map((url) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                    width: MediaQuery.of(context).size.width,
                    child: Image.asset("$url"));
              },
            );
          }).toList(),
        ),
      );

  indicatorSlide() => Positioned(
      bottom: widget.indicatorBottom,
      left: widget.indicatorLeft,
      right: widget.indicatorRigth,
      top: widget.indicatorTop,
      child: Row(
        children: widget.urlImageList.map((String url) {
          int index = widget.urlImageList.indexOf(url);
          Widget currentCircle = Container(
            width: 5,
            height: 5,
            margin: EdgeInsets.symmetric(horizontal: 3),
            decoration: BoxDecoration(
                border: Border.all(
                    color: Colors.white, width: 0.7, style: BorderStyle.solid),
                shape: BoxShape.circle,
                color: Colors.transparent),
          );

          Widget lineBlur = Container(
            height: 1,
            width: 5,
            margin: EdgeInsets.only(left: 4, right: 4),
            color: Colors.white,
          );

          return _currentImgIndex == index ? currentCircle : lineBlur;
        }).toList(),
      ));
}
