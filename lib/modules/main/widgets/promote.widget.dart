import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/fontsize.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/slide.image.widget.dart';

class PromoteWidget extends StatefulWidget {
  const PromoteWidget({Key? key}) : super(key: key);

  @override
  _PromoteWidgetState createState() => _PromoteWidgetState();
}

class _PromoteWidgetState extends State<PromoteWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      color: Colors.white,
      padding: EdgeInsets.all(5),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'สิ่งที่น่าสนใจ',
                  style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: FontSi.BASE,
                      fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                  child: Row(
                    children: [
                      Text(
                        'ดูเพิ่มเติม',
                        style: TextStyle(
                            color: Colors.grey.shade400, fontSize: FontSi.SM),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.grey.shade400,
                      )
                    ],
                  ),
                  onTap: () {
                    print('xxxxxxxxxxxxxx');
                  },
                )
              ],
            ),
          ),
          SlideImageWidget(
            aspectRatio: 3,
            buttonPadding: 1,
            indicatorBottom: 0,
            indicatorTop: 90,
            urlImageList: [
              "assets/images/banner1.jpeg",
              "assets/images/banner2.jpeg",
              "assets/images/banner3.png",
              "assets/images/banner4.png",
              "assets/images/banner5.png"
            ],
          )
        ],
      ),
    );
  }
}
