import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/custom.color.dart';
import 'package:flutter_shopee_clone/fontsize.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/slide.image.widget.dart';
import 'package:flutter_shopee_clone/utils/clippaths/discount.clippath.dart';
import 'package:flutter_shopee_clone/utils/clippaths/mall.clippath.dart';
import 'package:flutter_shopee_clone/utils/clippaths/topsale.clippath.dart';
import 'package:flutter_shopee_clone/utils/slidecountdown/slidecountdown.dart';
import 'package:fluttericon/entypo_icons.dart';
import 'package:fluttericon/font_awesome5_icons.dart';
import 'package:fluttericon/font_awesome_icons.dart';
import 'package:fluttericon/linearicons_free_icons.dart';

class FlashSalesWidget extends StatefulWidget {
  int maxItem = 10;
  List<dynamic> products = [];
  bool hasBorder;

  FlashSalesWidget(
      {required this.products, required this.maxItem, this.hasBorder = false});

  @override
  _FlashSalesWidgetState createState() => _FlashSalesWidgetState();
}

class _FlashSalesWidgetState extends State<FlashSalesWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      color: Colors.white,
      padding: EdgeInsets.all(5),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    children: [
                      Text(
                        'FLASH SALE',
                        style: TextStyle(
                            color: Colors.deepOrange,
                            fontSize: FontSi.BASE,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      SlideCountDownCustom(
                        duration:
                            Duration(hours: 7, minutes: 27, milliseconds: 40),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  child: Row(
                    children: [
                      Text(
                        'ดูเพิ่มเติม',
                        style: TextStyle(
                            color: Colors.grey.shade400, fontSize: FontSi.SM),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.grey.shade400,
                      )
                    ],
                  ),
                  onTap: () {
                    print('xxxxxxxxxxxxxx');
                  },
                )
              ],
            ),
          ),
          Container(
            height: 220,
            color: Colors.white,
            padding: EdgeInsets.all(3),
            child: SizedBox(
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: widget.maxItem,
                itemBuilder: (BuildContext context, int index) => Container(
                  height: 180,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(2),
                  child: (index + 1) == widget.maxItem
                      ? Column(
                          children: [
                            Container(
                              height: 195,
                              width: 130,
                              padding: EdgeInsets.all(1),
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      FontAwesome5.chevron_circle_right,
                                      color: Colors.deepOrange,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      'ดูเพิ่มเติม',
                                      style:
                                          TextStyle(color: Colors.deepOrange),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      : Column(
                          children: [
                            Container(
                              height: 155,
                              width: 155,
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    width: double.infinity,
                                    height: double.infinity,
                                    padding: EdgeInsets.all(2.5),
                                    decoration: widget.hasBorder
                                        ? BoxDecoration(
                                            border: Border.all(
                                              width: 1,
                                              color: Colors.grey.shade300,
                                            ),
                                          )
                                        : null,
                                    child: Image.asset(
                                      'assets/images/item2.jpeg',
                                      scale: 1,
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Column(
                                      children: [
                                        ClipPath(
                                          child: Container(
                                            alignment: Alignment.topCenter,
                                            padding: EdgeInsets.only(top: 5),
                                            width: 25,
                                            height: 35,
                                            color: Colors.deepOrange,
                                            child: Column(
                                              children: [
                                                Text(
                                                  'TOP',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: HColor.white,
                                                      fontSize: FontSi.XS),
                                                )
                                              ],
                                            ),
                                          ),
                                          clipper: TopSaleClipPath(),
                                        ),
                                        CustomPaint(
                                          painter: MallClipPath(),
                                          child: Stack(
                                            alignment: Alignment.topCenter,
                                            children: [
                                              Container(
                                                width: 25,
                                                height: 18,
                                                alignment: Alignment.topCenter,
                                                padding:
                                                    EdgeInsets.only(top: 1),
                                                child: Text(
                                                  'Mall',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: HColor.white,
                                                      fontSize: FontSi.XS),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: ClipPath(
                                      child: Container(
                                        alignment: Alignment.topCenter,
                                        padding: EdgeInsets.only(top: 1),
                                        width: 35,
                                        height: 40,
                                        color: Colors.yellow,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              'ลดx',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: HColor.white,
                                                  fontSize: FontSi.XS),
                                            ),
                                            Text(
                                              '24%',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.deepOrange,
                                                  fontSize: FontSi.BASE),
                                            ),
                                          ],
                                        ),
                                      ),
                                      clipper: DiscountClipPath(),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: 55,
                              width: 130,
                              decoration: widget.hasBorder
                                  ? BoxDecoration(
                                      border: Border.all(
                                        width: 1,
                                        color: Colors.grey.shade300,
                                      ),
                                    )
                                  : null,
                              padding: EdgeInsets.only(left: 5, right: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        '฿',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.red.shade700,
                                            fontSize: FontSi.SM,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        '6,345',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.red.shade700,
                                            fontSize: FontSi.LG,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    padding:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(15.0),
                                      child: Container(
                                        height: 12,
                                        width: 100,
                                        alignment: Alignment.center,
                                        child: Stack(
                                          children: [
                                            Container(
                                              color: Colors.grey.shade400,
                                            ),
                                            Container(
                                              width: 35,
                                              color: Colors.red.shade700,
                                            ),
                                            Center(
                                              child: Text(
                                                'ขายแล้ว 109',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: FontSi.XS2,
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
