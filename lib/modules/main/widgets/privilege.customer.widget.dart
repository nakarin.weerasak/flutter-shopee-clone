import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/fontsize.dart';
import 'package:flutter_shopee_clone/utils/clippaths/tickethead.clippath.dart';

class PrivilegeCustomerWidget extends StatefulWidget {
  const PrivilegeCustomerWidget({Key? key}) : super(key: key);

  @override
  _PrivilegeCustomerWidgetState createState() =>
      _PrivilegeCustomerWidgetState();
}

class _PrivilegeCustomerWidgetState extends State<PrivilegeCustomerWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: CustomPaint(
                painter: TicketHeadClippath(),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      height: 120,
                      width: double.infinity,
                      alignment: AlignmentDirectional.center,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'ฟรีค่าขนส่ง',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSi.BASE,
                                color: Colors.white),
                          ),
                          Text(
                            '0 บาท',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSi.XL3,
                                color: Colors.white),
                          ),
                          Container(
                            color: Colors.lightBlue.shade500,
                            padding: EdgeInsets.all(10),
                            child: Text(
                              'รองรับเก็บเงินปลายทาง',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: FontSi.XS,
                                  color: Colors.white),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 1,
                    color: Colors.grey.shade300,
                  ),
                ),
                width: double.infinity,
                height: 118.9,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      height: 120,
                      width: double.infinity,
                      alignment: AlignmentDirectional.topStart,
                      padding: EdgeInsets.all(5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'โค้ดส่งฟรี',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSi.XL,
                                color: Colors.grey.shade800),
                          ),
                          Text(
                            'ลดค่าขนส่งสูงสุด ฿40',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: FontSi.BASE,
                                color: Colors.grey.shade800),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: Colors.deepOrange,
                                  ),
                                ),
                                padding: EdgeInsets.all(4),
                                child: Text(
                                  'ลูกค้าใหม่',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: FontSi.XS,
                                      color: Colors.deepOrange),
                                ),
                              ),
                              Container(
                                color: Colors.deepOrange,
                                padding: EdgeInsets.all(8),
                                child: Text(
                                  'ช้อปเลย',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: FontSi.XS,
                                      color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text(
                              'ใช้ได้ภายใน 30 วันหลังจากได้รับโค้ด',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: FontSi.XS,
                                  color: Colors.grey.shade400),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}
