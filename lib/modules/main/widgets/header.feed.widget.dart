import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_shopee_clone/custom.color.dart';
import 'package:flutter_shopee_clone/fontsize.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/catigory.group.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/horizon.menu.widget.dart';
import 'package:fluttericon/font_awesome5_icons.dart';

class HeaderFeedWidget extends StatefulWidget {
  int itemActiveIndex;

  HeaderFeedWidget({this.itemActiveIndex = 2});

  @override
  _HeaderFeedWidgetState createState() => _HeaderFeedWidgetState();
}

class _HeaderFeedWidgetState extends State<HeaderFeedWidget> {
  final inputBorderRadius = OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.transparent,
      width: 0,
    ),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ),
  );
  late Color backgroundColorSearch;

  final double opacityMax = 0.01;
  late double opacity;
  late double offset;
  late double outScopeY = 190;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    backgroundColorSearch = Colors.white;
    opacity = 0.0;
    offset = 0.0;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(opacity > 0.5 ? 0.4 : 0),
              spreadRadius: 0.5,
              blurRadius: 0.4,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        child: Container(
          height: 80,
          color: Colors.white,
          child: SafeArea(
            bottom: false,
            child: Padding(
                padding: EdgeInsets.only(left: 8, right: 8),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                  left: 10, right: 5, top: 5, bottom: 5),
                              isDense: true,
                              enabledBorder: inputBorderRadius,
                              focusedBorder: inputBorderRadius,
                              hintText: "Feed",
                              hintStyle: TextStyle(
                                  color: Colors.grey.shade800,
                                  fontSize: FontSi.XL),
                              prefixIcon: Icon(
                                Icons.search,
                                size: 40,
                                color: Colors.deepOrange,
                              ),
                              prefixIconConstraints: BoxConstraints(
                                minWidth: 40,
                                minHeight: 40,
                              ),
                              filled: true,
                              fillColor: backgroundColorSearch,
                            ),
                          ),
                        ),
                        iconAnother(Icons.shopping_bag, 10, () => {}),
                        iconAnother(Icons.chat_bubble, 10, () => {})
                      ],
                    ),
                  ],
                )),
          ),
        ),
      ),
      SizedBox(
        height: 5,
      ),
      Container(
        decoration: BoxDecoration(color: Colors.white),
        height: 50,
        child: SizedBox(
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 10,
              itemBuilder: (BuildContext context, int index) => Container(
                    height: 30,
                    width: 100,
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(1),
                    child: InkWell(
                      child: Stack(
                        children: [
                          Container(
                            padding: EdgeInsets.only(top: 15),
                            height: double.infinity,
                            decoration: widget.itemActiveIndex == index
                                ? BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                          width: 2.0, color: Colors.deepOrange),
                                    ),
                                  )
                                : null,
                            child: Text(
                              '#Timeline',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: FontSi.BASE,
                                  color: widget.itemActiveIndex == index
                                      ? Colors.deepOrange
                                      : Colors.grey,
                                  fontWeight: widget.itemActiveIndex == index
                                      ? FontWeight.bold
                                      : null),
                            ),
                          ),
                        ],
                      ),
                      onTap: () {
                        setState(() {
                          widget.itemActiveIndex = index;
                        });
                      },
                    ),
                  )),
        ),
      ),
    ]);
  }

  Widget iconAnother(IconData icon, int countNotify, VoidCallback onPressed) {
    return Stack(
      children: [
        IconButton(
          onPressed: onPressed,
          icon: Icon(icon),
          iconSize: 24,
          color: Colors.deepOrange,
        ),
        countNotify <= 0
            ? Container()
            : Positioned(
                right: 0,
                child: Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: Colors.deepOrange,
                    borderRadius: BorderRadius.circular(16),
                    border: Border.all(color: Colors.white),
                  ),
                  constraints: BoxConstraints(minHeight: 20, minWidth: 20),
                  child: Text(
                    "$countNotify",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
      ],
    );
  }
}
