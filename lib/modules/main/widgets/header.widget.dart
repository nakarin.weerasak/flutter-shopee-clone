import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/catigory.group.widget.dart';
import 'package:flutter_shopee_clone/modules/main/widgets/horizon.menu.widget.dart';

class HeaderMainWidget extends StatefulWidget {
  TrackingScrollController trackingScrollController;

  HeaderMainWidget({required this.trackingScrollController});

  @override
  _HeaderMainWidgetState createState() => _HeaderMainWidgetState();
}

class _HeaderMainWidgetState extends State<HeaderMainWidget> {
  final inputBorderRadius = OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.transparent,
      width: 0,
    ),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ),
  );

  late Color backgroundColor;
  late Color backgroundColorSearch;
  late Color colorIcon;

  final double opacityMax = 0.01;
  late double opacity;
  late double offset;
  late double outScopeY = 190;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    backgroundColor = Colors.transparent;
    backgroundColorSearch = Colors.white;
    colorIcon = Colors.white;
    opacity = 0.0;
    offset = 0.0;

    widget.trackingScrollController.addListener(() {
      final scrollOffset = widget.trackingScrollController.offset;
      setState(() {
        if (widget.trackingScrollController.position.userScrollDirection ==
            ScrollDirection.reverse) {
          print('*** page scroll up');
          opacity += opacityMax;
          if (opacity >= 1.0) {
            opacity = 1.0;
          }
          if (scrollOffset >= outScopeY) {
            opacity = 1.0;
            colorIcon = Colors.deepOrange;
          }
        } else {
          print('*** page scroll down');
          if (scrollOffset <= outScopeY) {
            opacity -= opacityMax;
          }
        }

        if (scrollOffset > 10 && opacity > 0) {
          backgroundColorSearch = Colors.grey.shade100;
        } else {
          opacity = 0.0;
          offset = 0.0;
          backgroundColor = Colors.transparent;
          colorIcon = Colors.white;
          print('end top ***********************');
        }
        backgroundColor = Colors.white.withOpacity(opacity);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(opacity > 0.5 ? 0.4 : 0),
            spreadRadius: 0.5,
            blurRadius: 0.4,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Container(
        height: 100,
        color: backgroundColor,
        child: SafeArea(
          bottom: false,
          child: Padding(
            padding: EdgeInsets.only(left: 8, right: 8),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(5.0),
                      isDense: true,
                      enabledBorder: inputBorderRadius,
                      focusedBorder: inputBorderRadius,
                      hintText: "Shopee",
                      hintStyle: TextStyle(color: Colors.deepOrange),
                      prefixIcon: Icon(Icons.search),
                      prefixIconConstraints: BoxConstraints(
                        minWidth: 40,
                        minHeight: 40,
                      ),
                      suffixIcon: Icon(Icons.camera_alt),
                      suffixIconConstraints: BoxConstraints(
                        minWidth: 40,
                        minHeight: 40,
                      ),
                      filled: true,
                      fillColor: backgroundColorSearch,
                    ),
                  ),
                ),
                iconAnother(Icons.shopping_bag, 10, () => {}),
                iconAnother(Icons.chat_bubble, 10, () => {})
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget iconAnother(IconData icon, int countNotify, VoidCallback onPressed) {
    return Stack(
      children: [
        IconButton(
          onPressed: onPressed,
          icon: Icon(icon),
          iconSize: 24,
          color: colorIcon,
        ),
        countNotify <= 0
            ? Container()
            : Positioned(
                right: 0,
                child: Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: Colors.deepOrange,
                    borderRadius: BorderRadius.circular(16),
                    border: Border.all(color: Colors.white),
                  ),
                  constraints: BoxConstraints(minHeight: 20, minWidth: 20),
                  child: Text(
                    "$countNotify",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )
      ],
    );
  }
}
