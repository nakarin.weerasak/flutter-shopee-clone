import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/custom.color.dart';
import 'package:flutter_shopee_clone/fontsize.dart';

class CatigoryGroupWidget extends StatefulWidget {
  const CatigoryGroupWidget({Key? key}) : super(key: key);

  @override
  _CatigoryGroupWidgetState createState() => _CatigoryGroupWidgetState();
}

class _CatigoryGroupWidgetState extends State<CatigoryGroupWidget> {
  ScrollController gridviewScrollController = new ScrollController();

  double menuMiniScrollOffset = 0;
  double lineWidth = 35;
  double lineInnerWidth = 28;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    gridviewScrollController.addListener(() {
      double maxScroll = gridviewScrollController.position.maxScrollExtent;
      double lineConvertUnit = maxScroll / (lineWidth - lineInnerWidth / 2);

      setState(() {
        menuMiniScrollOffset =
            (gridviewScrollController.offset / (lineConvertUnit));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 10),
      padding: EdgeInsets.only(bottom: 10),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'หมวดหมู่',
                  style: TextStyle(
                      color: Colors.deepOrange,
                      fontSize: FontSi.BASE,
                      fontWeight: FontWeight.bold),
                ),
                GestureDetector(
                  child: Row(
                    children: [
                      Text(
                        'ดูเพิ่มเติม',
                        style: TextStyle(
                            color: Colors.grey.shade400, fontSize: FontSi.SM),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.grey.shade400,
                      )
                    ],
                  ),
                  onTap: () {
                    print('xxxxxxxxxxxxxx');
                  },
                )
              ],
            ),
          ),
          Container(
            height: 240,
            child: SizedBox(
              height: 78,
              child: new GridView.builder(
                  physics: const BouncingScrollPhysics(),
                  controller: gridviewScrollController,
                  scrollDirection: Axis.horizontal,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0.1,
                      mainAxisSpacing: 2),
                  shrinkWrap: true,
                  itemCount: 20,
                  itemBuilder: (context, index) {
                    return Container(
                        padding: EdgeInsets.only(left: 5, right: 5, top: 1),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Image.asset(
                                "assets/images/menus/menu_${(index + 1)}.png",
                                width: 80,
                                height: 80,
                              ),
                            ),
                            Container(
                              child: Text(
                                'ดีลใกล้ตัว เริ่มต้น 1.- ทดสอบรายการ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.grey.shade800,
                                    fontSize: FontSi.SM),
                              ),
                            )
                          ],
                        ));
                  }),
            ),
          ),
          SizedBox(
            height: 4,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: Container(
              height: 3.5,
              width: lineWidth,
              alignment: Alignment.center,
              child: Stack(
                children: [
                  Container(
                    color: Colors.grey.shade400,
                  ),
                  Transform(
                    transform:
                        Matrix4.translationValues(menuMiniScrollOffset, 0, 0.0),
                    child: Container(
                      width: lineInnerWidth / 2,
                      color: Colors.deepOrange,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
