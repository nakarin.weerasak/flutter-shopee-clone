import 'package:flutter/material.dart';
import 'package:sticky_headers/sticky_headers.dart';

class RecommendProductWidget extends StatefulWidget {
  const RecommendProductWidget({Key? key}) : super(key: key);

  @override
  _RecommendProductWidgetState createState() => _RecommendProductWidgetState();
}

class _RecommendProductWidgetState extends State<RecommendProductWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      color: Colors.amber,
      child: ListTile(
        leading: Text('ID'),
        title: Text('Name'),
        trailing: Text('Age'),
      ),
    );
  }
}
