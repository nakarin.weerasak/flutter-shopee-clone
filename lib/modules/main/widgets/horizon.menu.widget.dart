import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/custom.color.dart';
import 'package:flutter_shopee_clone/fontsize.dart';

class HorizontalMenuWidget extends StatefulWidget {
  const HorizontalMenuWidget({Key? key}) : super(key: key);

  @override
  _HorizontalMenuWidgetState createState() => _HorizontalMenuWidgetState();
}

class _HorizontalMenuWidgetState extends State<HorizontalMenuWidget> {
  ScrollController gridviewScrollController = new ScrollController();

  double menuMiniScrollOffset = 0;
  double lineWidth = 35;
  double lineInnerWidth = 28;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    gridviewScrollController.addListener(() {
      double maxScroll = gridviewScrollController.position.maxScrollExtent;
      double lineConvertUnit = maxScroll / (lineWidth - lineInnerWidth / 2);

      setState(() {
        menuMiniScrollOffset =
            (gridviewScrollController.offset / (lineConvertUnit));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(bottom: 10),
      child: Column(
        children: [
          Container(
            height: 180,
            child: SizedBox(
              height: 50,
              child: new GridView.builder(
                  physics: const BouncingScrollPhysics(),
                  controller: gridviewScrollController,
                  scrollDirection: Axis.horizontal,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 0.1,
                      mainAxisSpacing: 2),
                  shrinkWrap: true,
                  itemCount: 10,
                  itemBuilder: (context, index) {
                    return Container(
                        padding: EdgeInsets.only(left: 5, right: 5, top: 2),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left: 2, right: 2, top: 10, bottom: 5),
                              child: Image.asset(
                                "assets/images/icons/icon${(index + 1)}.png",
                                width: 45,
                                height: 45,
                              ),
                            ),
                            Container(
                              child: Text(
                                'ดีลใกล้ตัว เริ่มต้น 1.-',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.grey.shade800,
                                    fontSize: FontSi.XS),
                              ),
                            )
                          ],
                        ));
                  }),
            ),
          ),
          SizedBox(
            height: 4,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            child: Container(
              height: 3.5,
              width: lineWidth,
              alignment: Alignment.center,
              child: Stack(
                children: [
                  Container(
                    color: Colors.grey.shade400,
                  ),
                  Transform(
                    transform:
                        Matrix4.translationValues(menuMiniScrollOffset, 0, 0.0),
                    child: Container(
                      width: lineInnerWidth / 2,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
