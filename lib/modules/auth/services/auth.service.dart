import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/modules/auth/enums/mobile.verify.state.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future sendOTP({
    required String mobileNo,
    required Function(MobileVerifyStateEnum, String) callbackState,
    required Function(PhoneAuthCredential) callbackPhoneAuthCredential,
    required Function(String) errorCallback,
  }) async {
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: mobileNo,
          verificationCompleted:
              (PhoneAuthCredential phoneAuthCredential) async {
            print(
                'phoneAuthCredential **************** ${phoneAuthCredential.smsCode}');
          },
          verificationFailed: (FirebaseAuthException firebaseException) async {
            errorCallback(firebaseException.message.toString());
          },
          codeSent: (verificationId, resendingToken) async {
            callbackState(
                MobileVerifyStateEnum.SHOW_OTP_FORM_STATE, verificationId);
          },
          codeAutoRetrievalTimeout: (auto) async {});
      return null;
    } on FirebaseException catch (exception) {
      print('catch error ******* ${exception.toString()}');
      return Future.error(exception);
    }
  }

  Future verifyOTP({required verificationId, required String otp}) async {
    try {
      final PhoneAuthCredential phoneAuthCredential =
          PhoneAuthProvider.credential(
              verificationId: verificationId, smsCode: otp);
      return phoneAuthCredential;
    } on FirebaseException catch (exception) {
      return Future.error(exception);
    }
  }

  Future signInWithPhoneAuthCredential(
      {required PhoneAuthCredential credential}) async {
    try {
      final UserCredential authCredential =
          await _auth.signInWithCredential(credential);
      return authCredential;
    } on FirebaseException catch (exception) {
      return Future.error(exception);
    }
  }
}
