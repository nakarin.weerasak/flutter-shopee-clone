import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/modules/auth/enums/mobile.verify.state.dart';
import 'package:flutter_shopee_clone/modules/auth/services/auth.service.dart';
import 'package:flutter_shopee_clone/modules/landing/pages/landing.page.dart';
import 'package:flutter_shopee_clone/utils/alertdialog/alert.error.dart';

class AuthMobilePhonePage extends StatefulWidget {
  const AuthMobilePhonePage({Key? key}) : super(key: key);

  @override
  _AuthMobilePhonePageState createState() => _AuthMobilePhonePageState();
}

class _AuthMobilePhonePageState extends State<AuthMobilePhonePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  String verificationId = "";
  bool showLoading = false;
  MobileVerifyStateEnum mobileVerifyStateEnum =
      MobileVerifyStateEnum.SHOW_MOBILE_FORM_STATE;

  final textPhoneController = TextEditingController();

  final textChar1Controller = TextEditingController();
  final textChar2Controller = TextEditingController();
  final textChar3Controller = TextEditingController();
  final textChar4Controller = TextEditingController();
  final textChar5Controller = TextEditingController();
  final textChar6Controller = TextEditingController();

  bool showVerifyBTN = false;

  String mobilePhoneParse = "";
  String otpVal = "";

  @override
  void initState() {
    super.initState();
    textPhoneController.addListener(() {
      if (textPhoneController.text.length > 9) {
        setState(() {
          showVerifyBTN = true;
        });
      } else {
        setState(() {
          showVerifyBTN = false;
        });
      }
    });
  }

  void cleaAllOtpTextField() {
    textChar1Controller.clear();
    textChar2Controller.clear();
    textChar3Controller.clear();
    textChar4Controller.clear();
    textChar5Controller.clear();
    textChar6Controller.clear();
  }

  void sendOtpTOMobilePhone() {
    cleaAllOtpTextField();
    setState(() {
      showLoading = true;
      mobilePhoneParse = verifyThaiPhone(textPhoneController.text);
    });
    if (mobilePhoneParse.isNotEmpty) {
      AuthService().sendOTP(
          mobileNo: mobilePhoneParse,
          callbackState: (MobileVerifyStateEnum _mobileVerifyStateEnum,
              String _verificationId) async {
            setState(() {
              verificationId = _verificationId;
              mobileVerifyStateEnum = _mobileVerifyStateEnum;
              showLoading = false;
            });
          },
          callbackPhoneAuthCredential: (PhoneAuthCredential credential) async {
            print('credential ***** $credential');
          },
          errorCallback: (String error) {
            setState(() {
              showLoading = false;
            });
            print('error ****  $error');
            alertErrorMessage(context, error, () {});
          });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          backgroundColor: Colors.redAccent,
          content: Text('Mobile Phone Pattern Error'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        child: showLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Center(
                child: mobileVerifyStateEnum ==
                        MobileVerifyStateEnum.SHOW_MOBILE_FORM_STATE
                    ? Container(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextFormField(
                              controller: textPhoneController,
                              keyboardType: TextInputType.number,
                              maxLength: 10,
                              onChanged: (String v) {
                                print('Text Input ***** $v');
                              },
                              decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: 'Enter Mobile Phone'),
                            ),
                            showVerifyBTN
                                ? ElevatedButton(
                                    child: Text('Send OTP'),
                                    onPressed: () {
                                      sendOtpTOMobilePhone();
                                    },
                                  )
                                : Container()
                          ],
                        ),
                      )
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Text(
                              'Verify OTP',
                              style:
                                  TextStyle(color: Colors.purple, fontSize: 20),
                            ),
                          ),
                          SizedBox(
                            height: 28,
                          ),
                          Container(
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                _textFieldOTP(
                                    context: context,
                                    controller: textChar1Controller,
                                    first: true,
                                    last: false),
                                _textFieldOTP(
                                    context: context,
                                    controller: textChar2Controller,
                                    first: false,
                                    last: false),
                                _textFieldOTP(
                                    context: context,
                                    controller: textChar3Controller,
                                    first: false,
                                    last: false),
                                _textFieldOTP(
                                    context: context,
                                    controller: textChar4Controller,
                                    first: false,
                                    last: false),
                                _textFieldOTP(
                                    context: context,
                                    controller: textChar5Controller,
                                    first: false,
                                    last: false),
                                _textFieldOTP(
                                    context: context,
                                    controller: textChar6Controller,
                                    first: false,
                                    last: true)
                              ],
                            ),
                          ),
                          ElevatedButton(
                            child: Text(
                              'Resend Otp',
                              style: TextStyle(fontSize: 20),
                            ),
                            onPressed: () async {
                              sendOtpTOMobilePhone();
                            },
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          ElevatedButton(
                            child: Text(
                              'Confirm',
                              style: TextStyle(fontSize: 20),
                            ),
                            onPressed: () async {
                              try {
                                setState(() {
                                  otpVal =
                                      "${textChar1Controller.text}${textChar2Controller.text}${textChar3Controller.text}${textChar4Controller.text}${textChar5Controller.text}${textChar6Controller.text}";
                                  showLoading = true;
                                });
                                final PhoneAuthCredential phoneAuthCredential =
                                    await AuthService().verifyOTP(
                                        verificationId: verificationId,
                                        otp: otpVal);

                                final UserCredential userCredential =
                                    await AuthService()
                                        .signInWithPhoneAuthCredential(
                                            credential: phoneAuthCredential);

                                setState(() {
                                  showLoading = false;
                                });
                                // check have user profile
                                if (userCredential.user != null) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text('Auth Success'),
                                    ),
                                  );

                                  await Future.delayed(Duration(seconds: 2));

                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LandingPage()));
                                } else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      backgroundColor: Colors.redAccent,
                                      content: Text('Auth Fail 404'),
                                    ),
                                  );
                                }
                              } on FirebaseException catch (err) {
                                setState(() {
                                  showLoading = false;
                                });
                                final String errorMsg = err.toString();
                                print('errorMsg ** $errorMsg');

                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    backgroundColor: Colors.redAccent,
                                    content: Text("Auth Error"),
                                  ),
                                );

                                mobileVerifyStateEnum = MobileVerifyStateEnum
                                    .SHOW_MOBILE_FORM_STATE;
                              }
                            },
                          )
                        ],
                      )),
      ),
    );
  }
}

_textFieldOTP(
    {required context,
    required controller,
    required bool first,
    required bool last}) {
  return Container(
    height: 80,
    child: AspectRatio(
      aspectRatio: 0.6,
      child: TextField(
        controller: controller,
        autofocus: true,
        onChanged: (val) {
          if (val.length == 1 && last == false) {
            FocusScope.of(context).nextFocus();
          }
          if (val.length == 0 && first == false) {
            FocusScope.of(context).previousFocus();
          }
        },
        showCursor: false,
        readOnly: false,
        textAlign: TextAlign.center,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
        keyboardType: TextInputType.number,
        maxLength: 1,
        decoration: InputDecoration(
          counter: Offstage(),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 2, color: Colors.black12),
              borderRadius: BorderRadius.circular(12)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 2, color: Colors.purple),
              borderRadius: BorderRadius.circular(12)),
        ),
      ),
    ),
  );
}

String verifyThaiPhone(String mobileNo) {
  if (mobileNo.length == 10) {
    RegExp regExp = new RegExp(r"(06\w+|08\w+|09\w+)");
    if (regExp.hasMatch(mobileNo)) {
      return "+66${mobileNo.substring(1, 10)}";
    } else {
      return "";
    }
  } else {
    return "";
  }
}
