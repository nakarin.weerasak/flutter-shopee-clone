import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/modules/main/pages/feed.page.dart';
import 'package:flutter_shopee_clone/modules/main/pages/home.page%20copy.dart';
import 'package:flutter_shopee_clone/modules/main/pages/main.page.dart';
import 'package:flutter_shopee_clone/utils/controllers/navagation.controller.dart';
import 'package:flutter_shopee_clone/utils/navigationbar.bottom/navigationbar.dart';
import 'package:uuid/uuid.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  int menuselected = 1;
  Widget pageSelected = MainPage();
  NavigationControllerService navigationControllerService =
      new NavigationControllerService();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Stream navigationStream = navigationControllerService.init().stream;
    navigationStream.listen((indexSelect) {
      print('stream listen x ***** $indexSelect');
      if (indexSelect == 0) {
        pageSelected = MainPage();
      } else if (indexSelect == 1) {
        pageSelected = FeedPage();
      } else {
        pageSelected = MainPage();
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    navigationControllerService.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageSelected,
      bottomNavigationBar: SizedBox(
        child: bottomNavigationMenu(
          currentIndex: menuselected,
          currentIndexSelected: (int currentIndexSelected) => {
            setState(() {
              menuselected = currentIndexSelected;
              if (!currentIndexSelected.isNaN) {
                navigationControllerService.add(currentIndexSelected);
              }
            })
          },
        ),
        width: 40,
      ),
    );
  }
}
