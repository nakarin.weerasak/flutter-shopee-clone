import 'package:hexcolor/hexcolor.dart';

class HColor {
  static HexColor black = HexColor("#00183f");
  static HexColor gray = HexColor("#EEEEEE");
  static HexColor white = HexColor("#FDFEFE");
  static HexColor green = HexColor("#45B39D");
  static HexColor blue = HexColor("#85C1E9");
  static HexColor pink = HexColor("#FF8AB3");
  static HexColor red = HexColor("#E6055F");
  static HexColor yellow = HexColor("#FFD000");
}
