import 'package:flutter/material.dart';

bottomNavigationMenu(
    {required int currentIndex, required Function(int) currentIndexSelected}) {
  return BottomNavigationBar(
    type: BottomNavigationBarType.fixed,
    unselectedItemColor: Colors.black54,
    selectedFontSize: 12,
    unselectedFontSize: 12,
    items: [
      BottomNavigationBarItem(
        icon: Icon(
          Icons.home,
        ),
        label: 'หน้าแรก',
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.business,
        ),
        label: 'Feed',
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.school,
        ),
        label: 'Shopee Live',
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.school,
        ),
        label: 'การแจ้งเตือน',
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.school,
        ),
        label: 'ฉัน',
      )
    ],
    currentIndex: currentIndex,
    selectedItemColor: Colors.amber[800],
    onTap: (int select) {
      currentIndexSelected(select);
    },
  );
}
