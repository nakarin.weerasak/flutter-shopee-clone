import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class TicketHeadClippath extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_1 = Path();
    path_1.moveTo(size.width * 0.001473441, size.height * 0.5994344);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.5994344,
        size.width * 0.01843062,
        size.height * 0.5876796,
        size.width * 0.01843062,
        size.height * 0.5732027);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.5587249,
        size.width * 0.01083243,
        size.height * 0.5469701,
        size.width * 0.001473441,
        size.height * 0.5469701);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.5246346);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.5246346,
        size.width * 0.01843062,
        size.height * 0.5128798,
        size.width * 0.01843062,
        size.height * 0.4984019);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.4839252,
        size.width * 0.01083243,
        size.height * 0.4721704,
        size.width * 0.001473441,
        size.height * 0.4721704);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.4500695);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.4500695,
        size.width * 0.01843062,
        size.height * 0.4383147,
        size.width * 0.01843062,
        size.height * 0.4238369);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.4093602,
        size.width * 0.01083243,
        size.height * 0.3976054,
        size.width * 0.001473441,
        size.height * 0.3976054);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.3751503);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.3751503,
        size.width * 0.01843062,
        size.height * 0.3633955,
        size.width * 0.01843062,
        size.height * 0.3489177);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.3344410,
        size.width * 0.01083243,
        size.height * 0.3226862,
        size.width * 0.001473441,
        size.height * 0.3226862);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.2968913);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.2968913,
        size.width * 0.01843062,
        size.height * 0.2851376,
        size.width * 0.01843062,
        size.height * 0.2706597);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.2561819,
        size.width * 0.01083243,
        size.height * 0.2444280,
        size.width * 0.001473441,
        size.height * 0.2444280);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.2220914);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.2220914,
        size.width * 0.01843062,
        size.height * 0.2103366,
        size.width * 0.01843062,
        size.height * 0.1958599);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.1813820,
        size.width * 0.01083243,
        size.height * 0.1696273,
        size.width * 0.001473441,
        size.height * 0.1696273);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.1475264);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.1475264,
        size.width * 0.01843062,
        size.height * 0.1357716,
        size.width * 0.01843062,
        size.height * 0.1212937);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.1068170,
        size.width * 0.01083243,
        size.height * 0.09506221,
        size.width * 0.001473441,
        size.height * 0.09506221);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.07260719);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.07260719,
        size.width * 0.01843062,
        size.height * 0.06085240,
        size.width * 0.01843062,
        size.height * 0.04637566);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.03189783,
        size.width * 0.01083243,
        size.height * 0.02014304,
        size.width * 0.001473441,
        size.height * 0.02014304);
    path_1.lineTo(size.width * 0.001446592, size.height * 1.824485e-7);
    path_1.lineTo(size.width * 0.1233503, size.height * 1.824485e-7);
    path_1.lineTo(size.width * 0.1233772, size.height * 0.9999309);
    path_1.cubicTo(
        size.width * 0.1233772,
        size.height * 0.9999309,
        size.width * 0.01083243,
        size.height * 0.9994313,
        size.width * 0.001473441,
        size.height * 0.9994313);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.9769765);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.9769765,
        size.width * 0.01843062,
        size.height * 0.9652226,
        size.width * 0.01843062,
        size.height * 0.9507448);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.9362669,
        size.width * 0.01083243,
        size.height * 0.9245132,
        size.width * 0.001473441,
        size.height * 0.9245132);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.8987172);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.8987172,
        size.width * 0.01843062,
        size.height * 0.8869635,
        size.width * 0.01843062,
        size.height * 0.8724857);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.8580078,
        size.width * 0.01083243,
        size.height * 0.8462540,
        size.width * 0.001473441,
        size.height * 0.8462540);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.8239175);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.8239175,
        size.width * 0.01843062,
        size.height * 0.8121637,
        size.width * 0.01843062,
        size.height * 0.7976858);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.7832080,
        size.width * 0.01083243,
        size.height * 0.7714543,
        size.width * 0.001473441,
        size.height * 0.7714543);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.7493525);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.7493525,
        size.width * 0.01843062,
        size.height * 0.7375986,
        size.width * 0.01843062,
        size.height * 0.7231208);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.7086429,
        size.width * 0.01083243,
        size.height * 0.6968893,
        size.width * 0.001473441,
        size.height * 0.6968893);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.6744333);
    path_1.cubicTo(
        size.width * 0.01083243,
        size.height * 0.6744333,
        size.width * 0.01843062,
        size.height * 0.6626794,
        size.width * 0.01843062,
        size.height * 0.6482016);
    path_1.cubicTo(
        size.width * 0.01843062,
        size.height * 0.6337238,
        size.width * 0.01083243,
        size.height * 0.6219701,
        size.width * 0.001473441,
        size.height * 0.6219701);
    path_1.lineTo(size.width * 0.001473441, size.height * 0.5994335);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Color(0xffb5b5b5).withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.002722254, size.height * 0.5994344);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.5994344,
        size.width * 0.01967944,
        size.height * 0.5876796,
        size.width * 0.01967944,
        size.height * 0.5732027);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.5587249,
        size.width * 0.01208124,
        size.height * 0.5469701,
        size.width * 0.002722254,
        size.height * 0.5469701);
    path_2.lineTo(size.width * 0.002722254, size.height * 0.5246346);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.5246346,
        size.width * 0.01967944,
        size.height * 0.5128798,
        size.width * 0.01967944,
        size.height * 0.4984019);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.4839252,
        size.width * 0.01208124,
        size.height * 0.4721704,
        size.width * 0.002722254,
        size.height * 0.4721704);
    path_2.lineTo(size.width * 0.002722254, size.height * 0.4500695);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.4500695,
        size.width * 0.01967944,
        size.height * 0.4383147,
        size.width * 0.01967944,
        size.height * 0.4238369);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.4093602,
        size.width * 0.01208124,
        size.height * 0.3976054,
        size.width * 0.002722254,
        size.height * 0.3976054);
    path_2.lineTo(size.width * 0.002722254, size.height * 0.3751503);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.3751503,
        size.width * 0.01967944,
        size.height * 0.3633955,
        size.width * 0.01967944,
        size.height * 0.3489177);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.3344410,
        size.width * 0.01208124,
        size.height * 0.3226862,
        size.width * 0.002722254,
        size.height * 0.3226862);
    path_2.lineTo(size.width * 0.002722254, size.height * 0.2968913);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.2968913,
        size.width * 0.01967944,
        size.height * 0.2851376,
        size.width * 0.01967944,
        size.height * 0.2706597);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.2561819,
        size.width * 0.01208124,
        size.height * 0.2444280,
        size.width * 0.002722254,
        size.height * 0.2444280);
    path_2.lineTo(size.width * 0.002722254, size.height * 0.2220914);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.2220914,
        size.width * 0.01967944,
        size.height * 0.2103366,
        size.width * 0.01967944,
        size.height * 0.1958599);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.1813820,
        size.width * 0.01208124,
        size.height * 0.1696273,
        size.width * 0.002722254,
        size.height * 0.1696273);
    path_2.lineTo(size.width * 0.002722254, size.height * 0.1475264);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.1475264,
        size.width * 0.01967944,
        size.height * 0.1357716,
        size.width * 0.01967944,
        size.height * 0.1212937);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.1068170,
        size.width * 0.01208124,
        size.height * 0.09506221,
        size.width * 0.002722254,
        size.height * 0.09506221);
    path_2.lineTo(size.width * 0.002722254, size.height * 0.07260719);
    path_2.cubicTo(
        size.width * 0.01208124,
        size.height * 0.07260719,
        size.width * 0.01967944,
        size.height * 0.06085240,
        size.width * 0.01967944,
        size.height * 0.04637566);
    path_2.cubicTo(
        size.width * 0.01967944,
        size.height * 0.03189783,
        size.width * 0.01208124,
        size.height * 0.02014304,
        size.width * 0.002722254,
        size.height * 0.02014304);
    path_2.lineTo(size.width * 0.002695406, size.height * 1.824485e-7);
    path_2.lineTo(size.width * 0.1245991, size.height * 1.824485e-7);
    path_2.lineTo(size.width * 0.1246259, size.height * 0.9999309);
    path_2.cubicTo(
        size.width * 0.1246259,
        size.height * 0.9999309,
        size.width * 0.01208124,
        size.height * 0.9994313,
        size.width * 0.002722139,
        size.height * 0.9994313);
    path_2.lineTo(size.width * 0.002722139, size.height * 0.9769765);
    path_2.cubicTo(
        size.width * 0.01208112,
        size.height * 0.9769765,
        size.width * 0.01967932,
        size.height * 0.9652226,
        size.width * 0.01967932,
        size.height * 0.9507448);
    path_2.cubicTo(
        size.width * 0.01967932,
        size.height * 0.9362669,
        size.width * 0.01208112,
        size.height * 0.9245132,
        size.width * 0.002722139,
        size.height * 0.9245132);
    path_2.lineTo(size.width * 0.002722139, size.height * 0.8987172);
    path_2.cubicTo(
        size.width * 0.01208112,
        size.height * 0.8987172,
        size.width * 0.01967932,
        size.height * 0.8869635,
        size.width * 0.01967932,
        size.height * 0.8724857);
    path_2.cubicTo(
        size.width * 0.01967932,
        size.height * 0.8580078,
        size.width * 0.01208112,
        size.height * 0.8462540,
        size.width * 0.002722139,
        size.height * 0.8462540);
    path_2.lineTo(size.width * 0.002722139, size.height * 0.8239175);
    path_2.cubicTo(
        size.width * 0.01208112,
        size.height * 0.8239175,
        size.width * 0.01967932,
        size.height * 0.8121637,
        size.width * 0.01967932,
        size.height * 0.7976858);
    path_2.cubicTo(
        size.width * 0.01967932,
        size.height * 0.7832080,
        size.width * 0.01208112,
        size.height * 0.7714543,
        size.width * 0.002722139,
        size.height * 0.7714543);
    path_2.lineTo(size.width * 0.002722139, size.height * 0.7493525);
    path_2.cubicTo(
        size.width * 0.01208112,
        size.height * 0.7493525,
        size.width * 0.01967932,
        size.height * 0.7375986,
        size.width * 0.01967932,
        size.height * 0.7231208);
    path_2.cubicTo(
        size.width * 0.01967932,
        size.height * 0.7086429,
        size.width * 0.01208112,
        size.height * 0.6968893,
        size.width * 0.002722139,
        size.height * 0.6968893);
    path_2.lineTo(size.width * 0.002722139, size.height * 0.6744333);
    path_2.cubicTo(
        size.width * 0.01208112,
        size.height * 0.6744333,
        size.width * 0.01967932,
        size.height * 0.6626794,
        size.width * 0.01967932,
        size.height * 0.6482016);
    path_2.cubicTo(
        size.width * 0.01967932,
        size.height * 0.6337238,
        size.width * 0.01208112,
        size.height * 0.6219701,
        size.width * 0.002722139,
        size.height * 0.6219701);
    path_2.lineTo(size.width * 0.002722139, size.height * 0.5994335);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.deepOrange;
    canvas.drawPath(path_2, paint_2_fill);

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.deepOrange;
    canvas.drawRect(
        Rect.fromLTWH(size.width * 0.1097629, 0, size.width * 0.8916179,
            size.height * 0.9999307),
        paint_3_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
