import 'package:flutter/material.dart';

class MallClipPath extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    Path path = Path();

    // Path number 1

    paint.color = Color(0xff8c0100);
    path = Path();
    path.lineTo(size.width * 0.08, 0);
    path.cubicTo(size.width * 0.08, 0, 0, 0, 0, 0);
    path.cubicTo(0, 0, 0, size.height * 0.7, 0, size.height * 0.7);
    path.cubicTo(
        0, size.height * 0.7, 0, size.height * 0.7, 0, size.height * 0.7);
    path.cubicTo(0, size.height * 0.7, size.width * 0.1, size.height,
        size.width * 0.1, size.height);
    path.cubicTo(size.width * 0.1, size.height, size.width * 0.1,
        size.height * 0.7, size.width * 0.1, size.height * 0.7);
    path.cubicTo(size.width * 0.1, size.height * 0.7, size.width,
        size.height * 0.36, size.width, size.height * 0.36);
    path.cubicTo(size.width, size.height * 0.36, size.width * 0.08, 0,
        size.width * 0.08, 0);
    path.cubicTo(
        size.width * 0.08, 0, size.width * 0.08, 0, size.width * 0.08, 0);
    canvas.drawPath(path, paint);

    // Path number 2

    paint.color = Color(0xffe40806);
    path = Path();
    path.lineTo(size.width * 0.98, size.height * 0.15);
    path.cubicTo(size.width * 0.98, size.height * 0.11, size.width * 0.98,
        size.height * 0.07, size.width * 0.97, size.height * 0.04);
    path.cubicTo(size.width * 0.96, size.height * 0.02, size.width * 0.94, 0,
        size.width * 0.93, 0);
    path.cubicTo(size.width * 0.93, 0, 0, 0, 0, 0);
    path.cubicTo(0, 0, 0, size.height * 0.7, 0, size.height * 0.7);
    path.cubicTo(0, size.height * 0.7, size.width * 0.93, size.height * 0.7,
        size.width * 0.93, size.height * 0.7);
    path.cubicTo(size.width * 0.94, size.height * 0.7, size.width * 0.96,
        size.height * 0.69, size.width * 0.97, size.height * 0.66);
    path.cubicTo(size.width * 0.98, size.height * 0.63, size.width * 0.98,
        size.height * 0.59, size.width * 0.98, size.height * 0.55);
    path.cubicTo(size.width * 0.98, size.height * 0.55, size.width * 0.98,
        size.height * 0.15, size.width * 0.98, size.height * 0.15);
    path.cubicTo(size.width * 0.98, size.height * 0.15, size.width * 0.98,
        size.height * 0.15, size.width * 0.98, size.height * 0.15);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
