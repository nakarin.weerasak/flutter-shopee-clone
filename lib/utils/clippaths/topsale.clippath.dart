import 'package:flutter/material.dart';

class TopSaleClipPath extends CustomClipper<Path> {
  var radius = 10.0;
  @override
  Path getClip(Size size) {
    return Path()
      ..lineTo(0.0, size.height / 2)
      ..lineTo(size.width / 2, size.height - (size.height / 5))
      ..lineTo(size.width, (size.height / 2))
      ..lineTo(size.width, 0.0)
      ..close();
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
