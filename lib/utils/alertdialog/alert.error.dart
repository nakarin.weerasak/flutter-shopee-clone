import 'package:flutter/material.dart';
import 'package:flutter_shopee_clone/custom.color.dart';
import 'package:flutter_shopee_clone/fontsize.dart';

void alertErrorMessage(context, String errMessage, Function callback) {
  final _width = MediaQuery.of(context).size.width;
  final _height = MediaQuery.of(context).size.height;

  showDialog(
    barrierDismissible: true,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        contentPadding: EdgeInsets.only(top: 5.0),
        content: new Container(
          decoration: new BoxDecoration(
            shape: BoxShape.rectangle,
            color: const Color(0xFFFFFF),
            borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                alignment: Alignment.center,
                child: Text(
                  'Error!',
                  style: TextStyle(
                      fontSize: FontSi.XL1,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
                width: _width,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                errMessage,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: FontSi.LG),
              ),
              SizedBox(
                height: 40,
              ),
              Container(
                width: _width,
                padding: EdgeInsets.only(left: 10, right: 10, bottom: 5),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('OK'),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
