import 'package:flutter/material.dart';

class PullToRefresh extends StatefulWidget {
  final Future Function() onRefresh;
  final Widget child;

  const PullToRefresh({Key? key, required this.child, required this.onRefresh})
      : super(key: key);

  @override
  _PullToRefreshState createState() => _PullToRefreshState();
}

class _PullToRefreshState extends State<PullToRefresh> {
  @override
  Widget build(BuildContext context) => pullRefresh();

  Widget pullRefresh() => RefreshIndicator(
      child: widget.child, onRefresh: widget.onRefresh, displacement: 80);
}
