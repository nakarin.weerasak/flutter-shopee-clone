import 'dart:async';

class NavigationControllerService {
  late StreamController<int> navigationController;
  StreamController init() {
    navigationController = StreamController<int>.broadcast();
    return navigationController;
  }

  add(int value) {
    print('NavigationControllerService **** [add]');
    navigationController.add(value);
  }

  close() {
    print('NavigationControllerService **** [close]');
    navigationController.close();
  }
}
