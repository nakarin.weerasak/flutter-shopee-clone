import 'package:flutter/material.dart';
import 'package:slide_countdown_clock/slide_countdown_clock.dart';

class SlideCountDownCustom extends StatelessWidget {
  Duration duration = Duration(seconds: 1000000);
  SlideCountDownCustom({required this.duration});

  @override
  Widget build(BuildContext context) {
    return SlideCountdownClock(
      duration: duration,
      slideDirection: SlideDirection.Up,
      separator: ":",
      textStyle: TextStyle(
        fontSize: 12,
        wordSpacing: 1,
        fontWeight: FontWeight.bold,
        color: Colors.white,
      ),
      separatorTextStyle: TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.bold,
        color: Colors.grey.shade900,
      ),
      padding: EdgeInsets.only(left: 0.5, right: 0.5, top: 1, bottom: 1),
      decoration: BoxDecoration(color: Colors.grey.shade900),
      onDone: () {
        print('count down done **');
      },
    );
  }
}
