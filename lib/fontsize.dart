class FontSi {
  static const double XS4 = 4;
  static const double XS3 = 6;
  static const double XS2 = 8;
  static const double XS = 10;
  static const double SM = 12;
  static const double BASE = 14;
  static const double LG = 16;
  static const double XL = 18;
  static const double XL1 = 20;
  static const double XL2 = 22;
  static const double XL3 = 24;
}
